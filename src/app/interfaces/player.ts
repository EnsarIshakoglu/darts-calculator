import {Turn} from './turn';

export interface Player {
  turns: Turn[];
  name: string;
  remainingPoints: number;
}
