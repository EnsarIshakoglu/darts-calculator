export interface Throw {
  score: number;
  scoreText: string;
}
