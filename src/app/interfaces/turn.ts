import {Throw} from './throw';

export interface Turn {
  throws: Throw[];
  totalScore: number;
}
