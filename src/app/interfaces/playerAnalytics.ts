export interface PlayerAnalytics {
  averageThrowScore: number;
  averageTurnScore: number;
  thrownScoreAmount: {};
}
