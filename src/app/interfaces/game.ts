import { gameModes } from '../models/game-modes';

export interface Game {
  mode: gameModes;
  gameReady: boolean;
  totalPoints: number;
  board: number[][];
}
