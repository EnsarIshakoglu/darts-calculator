import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { StandardGameComponent } from './standard-game/standard-game.component';
import { PlayerComponent } from './player/player.component';
import {FormsModule} from '@angular/forms';
import {Range} from './pipes/Range';
import { GameAnalyticsComponent } from './game-analytics/game-analytics.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    StandardGameComponent,
    PlayerComponent,
    Range,
    GameAnalyticsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
