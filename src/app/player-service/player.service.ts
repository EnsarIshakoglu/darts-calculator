import {Injectable} from '@angular/core';
import {Player} from '../interfaces/player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor() { }

  createPlayer(): Player{
    return {
      name: '',
      remainingPoints: 0,
      turns: []
    } as Player;
  }
}
