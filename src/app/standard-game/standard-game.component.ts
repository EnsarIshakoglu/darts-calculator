import {Component, OnInit} from '@angular/core';
import {Game} from '../interfaces/game';
import {gameModes} from '../models/game-modes';
import {Player} from '../interfaces/player';
import {PlayerService} from '../player-service/player.service';
import {Throw} from '../interfaces/throw';
import ThrowMultipliers from '../enums/throwMultipliers';
import {Turn} from '../interfaces/turn';
import {TurnService} from '../turn-service/turn.service';

@Component({
  selector: 'app-standard-game',
  templateUrl: './standard-game.component.html',
  styleUrls: ['./standard-game.component.scss']
})
export class StandardGameComponent implements OnInit, Game {

  mode: gameModes.standard;
  players: Player[] = [];
  currentPlayer: Player;
  gameReady = false;
  showAnalytics = false;
  totalPoints = 501;
  board = [
    [1, 2, 3, 4, 5],
    [6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15],
    [16, 17, 18, 19, 20]
  ];
  multiplier: ThrowMultipliers = ThrowMultipliers.SINGLE;
  bigNotificationVisible = false;
  bigNotificationText = '';
  playerService: PlayerService;
  turnService: TurnService;

  constructor(playerService: PlayerService, turnService: TurnService) {
    this.players[0] = playerService.createPlayer();
    this.players[1] = playerService.createPlayer();

    this.currentPlayer = this.players[0];

    this.playerService = playerService;
    this.turnService = turnService;
  }

  ngOnInit(): void {
  }

  public get throwMultipliers(): typeof ThrowMultipliers {
    return ThrowMultipliers;
  }

  initGame(): void {
    if (!this.players[0].name || !this.players[1].name){
      alert('Please fill in both player names!');
      return;
    }

    if (this.players[0].name === this.players[1].name){
      this.players[1].name += '(2)';
    }
    
    this.players[0].remainingPoints = this.totalPoints;
    this.players[1].remainingPoints = this.totalPoints;

    this.currentPlayer = this.players[0];

    this.gameReady = true;
  }

  multiplierClicked(multiplier: ThrowMultipliers): void{
    this.multiplier = (this.multiplier === multiplier) ? 1 : multiplier;
  }

  getMultiplierText(): string{
    return this.multiplier === ThrowMultipliers.SINGLE ? '' : ThrowMultipliers[this.multiplier].charAt(0);
  }

  getDartThrow(score: number): Throw{
    let dartThrow: Throw;
    if (score === 25 || score === 50) {
      dartThrow = {
        score,
        scoreText: `${score}`
      };
    } else {
      dartThrow = {
        score: score * this.multiplier,
        scoreText: `${this.getMultiplierText()}${score}`
      };
    }

    return dartThrow;
  }

  addShot(score: number): void {
    const player = this.currentPlayer;

    if (player.turns.length === 0 || player.turns[player.turns.length - 1].throws.length === 3){
      player.turns.push(this.turnService.createTurn());
    }

    const playerTurn = player.turns[player.turns.length - 1];

    const dartThrow = this.getDartThrow(score);

    if (player.remainingPoints < dartThrow.score){
      this.handleNoScore(player);
      return;
    }

    player.remainingPoints -= dartThrow.score;
    playerTurn.throws.push(dartThrow);
    playerTurn.totalScore += dartThrow.score;

    this.resetMultiplier();

    if (player.remainingPoints === 0){
      this.setGameFinished(player);
      this.showAnalytics = true;
    }

    if (playerTurn.throws.length === 3){
      this.currentPlayer = this.getOtherPlayer(this.currentPlayer);
    }
  }

  setGameFinished(player: Player): void{
    this.showBigNotification(`${player.name} has won!`, 1000);
  }

  resetGameData(): void{
    this.gameReady = false;
    this.showAnalytics = false;
    this.totalPoints = 501;
    this.currentPlayer = this.playerService.createPlayer();

    this.players[0] = this.playerService.createPlayer();
    this.players[1] = this.playerService.createPlayer();
  }

  undoShot(): void {
    const currentPlayer = this.currentPlayer;
    const currentPlayerTurn: Turn = currentPlayer.turns[currentPlayer.turns.length - 1];

    let playerToUndoShot: Player;

    if (currentPlayer.turns.length === 0 || currentPlayerTurn.throws.length % 3 === 0){
      playerToUndoShot = this.getOtherPlayer(currentPlayer);
    } else {
      playerToUndoShot = currentPlayer;
    }

    const playerToUndoShotTurn: Turn = playerToUndoShot.turns[playerToUndoShot.turns.length - 1];

    if (playerToUndoShotTurn.throws.length === 0) {
      return;
    }

    const shotScore = playerToUndoShotTurn.throws.pop().score;
    if (playerToUndoShotTurn.throws.length === 0) {
      playerToUndoShot.turns.pop();
    }
    playerToUndoShot.remainingPoints += shotScore;
    playerToUndoShotTurn.totalScore -= shotScore;

    this.currentPlayer = playerToUndoShot;
  }

  getOtherPlayer(player: Player): Player{
    return player.name === this.players[0].name ? this.players[1] : this.players[0];
  }

  resetMultiplier(): void{
    this.multiplier = 1;
  }

  handleNoScore(player: Player): void{
    const turn = player.turns.pop();

    turn.throws.forEach((dartThrow) => {
      player.remainingPoints += dartThrow.score;
    });

    const noScoreThrow: Throw = {
      score: 0,
      scoreText: '0'
    };

    const noScoreTurn: Turn = {
      throws: [noScoreThrow, noScoreThrow, noScoreThrow],
      totalScore: 0
    };

    player.turns.push(noScoreTurn);

    this.currentPlayer = this.getOtherPlayer(player);

    this.showBigNotification('NO SCORE', 1500);
  }

  showBigNotification(text: string, duration: number): void{
    this.bigNotificationText = text;
    this.bigNotificationVisible = true;

    setTimeout(() => this.bigNotificationVisible = false, duration);
  }

}
