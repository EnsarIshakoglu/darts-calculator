import { Injectable } from '@angular/core';
import {Turn} from '../interfaces/turn';

@Injectable({
  providedIn: 'root'
})
export class TurnService {

  constructor() { }

  createTurn(): Turn{
    return {
      throws: [],
      totalScore: 0
    };
  }
}
