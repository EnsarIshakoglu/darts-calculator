import { TestBed } from '@angular/core/testing';

import { ThrowoutLibraryService } from './throwout-library.service';

describe('ThrowoutServiceService', () => {
  let service: ThrowoutLibraryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThrowoutLibraryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
