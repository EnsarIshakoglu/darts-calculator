import {AfterViewChecked, Component, ElementRef, Host, Input, OnInit, ViewChild} from '@angular/core';
import {Player} from '../interfaces/player';
import {ThrowoutLibraryService} from '../throwout-service/throwout-library.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, AfterViewChecked {
  @ViewChild('shotHistoryList') private scrollContainer: ElementRef;

  @Input() player: Player;
  private throwOutService: ThrowoutLibraryService;

  constructor(throwOutService: ThrowoutLibraryService) {
    this.throwOutService = throwOutService;
  }

  ngOnInit(): void {
  }

  ngAfterViewChecked(): void {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  getThrowOuts(): string[]{
    return this.throwOutService.getThrowOutsFromScore(this.player.remainingPoints);
  }

  getShotsLeft(): number{
    const playerTurn = this.player.turns[this.player.turns.length - 1] || null;
    const turnThrows = playerTurn ? playerTurn.throws.length : 0;
    return turnThrows === 3 ? 3 : 3 - turnThrows;
  }
}
