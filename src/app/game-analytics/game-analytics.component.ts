import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Player} from '../interfaces/player';
import {PlayerService} from '../player-service/player.service';
import {PlayerAnalytics} from '../interfaces/playerAnalytics';

@Component({
  selector: 'app-game-analytics',
  templateUrl: './game-analytics.component.html',
  styleUrls: ['./game-analytics.component.scss']
})
export class GameAnalyticsComponent implements OnInit {

  @Input() players: Player[];
  @Output() resetGame = new EventEmitter();

  playerService: PlayerService;

  player1: PlayerAnalytics;
  player2: PlayerAnalytics;

  constructor(playerService: PlayerService) {
    this.playerService = playerService;
  }

  ngOnInit(): void {
    this.player1 = {
      averageThrowScore: this.calculateAverageThrowScore(this.players[0]),
      averageTurnScore: this.calculateAverageTurnScore(this.players[0]),
      thrownScoreAmount: this.getThrownScoreAmount(this.players[0])
    };

    this.player2 = {
      averageThrowScore: this.calculateAverageThrowScore(this.players[1]),
      averageTurnScore: this.calculateAverageTurnScore(this.players[1]),
      thrownScoreAmount: this.getThrownScoreAmount(this.players[1])
    };

  }

  calculateAverageThrowScore(player: Player): number{
    const thrownScore = 501 - player.remainingPoints;
    const thrownAmount = ((player.turns.length - 1) * 3) + player.turns[player.turns.length - 1].throws.length;

    return Math.round(((thrownScore / thrownAmount) + Number.EPSILON) * 100) / 100;
  }

  calculateAverageTurnScore(player: Player): number{
    const thrownScore = 501 - player.remainingPoints;

    return Math.round(((thrownScore / player.turns.length) + Number.EPSILON) * 100) / 100;
  }

  getThrownScoreAmount(player: Player): any{
    const thrownScoreAmount = {};
    player.turns.forEach(turn => {
      console.log(turn);
      turn.throws.forEach(dartThrow => {
        if (thrownScoreAmount[`${dartThrow.scoreText}`]){
          thrownScoreAmount[`${dartThrow.scoreText}`]++;
        } else {
          thrownScoreAmount[`${dartThrow.scoreText}`] = 1;
        }
      });
    });

    return thrownScoreAmount;
  }

  closeAnalytics(): void{
    this.resetGame.emit();
  }

}
